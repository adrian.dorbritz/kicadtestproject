EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title "hierarchical top "
Date "2020-02-02"
Rev "1"
Comp "e.lective GmbH"
Comment1 "Berlin"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3775 6850 4075 6850
Text Label 775  900  2    50   ~ 0
Input
$Comp
L power:VCC #PWR01
U 1 1 5E3B9B3F
P 1275 6850
F 0 "#PWR01" H 1275 6700 50  0001 C CNN
F 1 "VCC" V 1292 6978 50  0000 L CNN
F 2 "" H 1275 6850 50  0001 C CNN
F 3 "" H 1275 6850 50  0001 C CNN
	1    1275 6850
	0    1    1    0   
$EndComp
Wire Wire Line
	1275 6850 1200 6850
Wire Wire Line
	1200 7175 1350 7175
Wire Wire Line
	1350 7175 1350 7200
$Comp
L power:GND #PWR02
U 1 1 5E3BA788
P 1350 7200
F 0 "#PWR02" H 1350 6950 50  0001 C CNN
F 1 "GND" H 1355 7027 50  0000 C CNN
F 2 "" H 1350 7200 50  0001 C CNN
F 3 "" H 1350 7200 50  0001 C CNN
	1    1350 7200
	1    0    0    -1  
$EndComp
Text Label 4075 6850 0    50   ~ 0
Input
Text Label 1900 900  0    50   ~ 0
Output
Wire Wire Line
	1700 900  1900 900 
Wire Notes Line
	525  6350 6875 6350
Wire Notes Line
	6875 6350 6875 7800
Text Notes 525  6525 0    100  ~ 0
Supplies
Text Notes 3050 6525 0    100  ~ 0
Generators
Wire Notes Line
	2975 6350 2975 7800
Wire Wire Line
	775  900  1050 900 
$Sheet
S 1050 775  650  250 
U 5E3B4D4D
F0 "AmplificationStage" 50
F1 "Modules/AmplificationStage.sch" 50
F2 "Input" I L 1050 900 50 
F3 "Output" I R 1700 900 50 
$EndSheet
$Sheet
S 700  6725 500  625 
U 5E3ADA3B
F0 "TestFixturePowerSupply" 50
F1 "Modules/TestFixturePowerSupply.sch" 50
F2 "Ground" I R 1200 7175 50 
F3 "Supply" I R 1200 6850 50 
$EndSheet
$Sheet
S 3200 6725 575  375 
U 5E3B08BC
F0 "SinusTestSignalGenerator" 50
F1 "Modules/SinusTestSignalGenerator.sch" 50
F2 "SinusOutput" I R 3775 6850 50 
$EndSheet
$EndSCHEMATC
