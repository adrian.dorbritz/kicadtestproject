EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp "e.lective GmbH"
Comment1 "Berlin"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	1475 1475 1225 1475
Wire Wire Line
	1225 1475 1225 1675
$Comp
L power:GND #PWR?
U 1 1 5E3B0FF0
P 1225 2175
AR Path="/5E3B0FF0" Ref="#PWR?"  Part="1" 
AR Path="/5E3B08BC/5E3B0FF0" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 1225 1925 50  0001 C CNN
F 1 "GND" H 1230 2002 50  0000 C CNN
F 2 "" H 1225 2175 50  0001 C CNN
F 3 "" H 1225 2175 50  0001 C CNN
	1    1225 2175
	1    0    0    -1  
$EndComp
$Comp
L Simulation_SPICE:VSIN V?
U 1 1 5E3B0FF9
P 1225 1875
AR Path="/5E3B0FF9" Ref="V?"  Part="1" 
AR Path="/5E3B08BC/5E3B0FF9" Ref="V2"  Part="1" 
F 0 "V2" H 1355 1966 50  0000 L CNN
F 1 "VSIN" H 1355 1875 50  0000 L CNN
F 2 "Connectors:BNC" H 1225 1875 50  0001 C CNN
F 3 "~" H 1225 1875 50  0001 C CNN
F 4 "Y" H 1225 1875 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 1225 1875 50  0001 L CNN "Spice_Primitive"
F 6 "sin(0 1 2)" H 1355 1784 50  0000 L CNN "Spice_Model"
	1    1225 1875
	1    0    0    -1  
$EndComp
Wire Wire Line
	1225 2075 1225 2175
Text HLabel 1475 1475 2    50   Input ~ 0
SinusOutput
$EndSCHEMATC
