EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp "e.lective GmbH"
Comment1 "Berlin"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Simulation_SPICE:VDC V1
U 1 1 5E3AF33A
P 1425 1450
F 0 "V1" H 1555 1541 50  0000 L CNN
F 1 "10V" H 1555 1450 50  0000 L CNN
F 2 "Connectors:Banana_Jack_2Pin" H 1425 1450 50  0001 C CNN
F 3 "~" H 1425 1450 50  0001 C CNN
F 4 "Y" H 1425 1450 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 1425 1450 50  0001 L CNN "Spice_Primitive"
F 6 "dc 10" H 1555 1359 50  0000 L CNN "Spice_Model"
	1    1425 1450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1425 1650 1425 1750
Wire Wire Line
	1425 1100 1425 1150
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5E3AF34E
P 1650 1100
F 0 "#FLG01" H 1650 1175 50  0001 C CNN
F 1 "PWR_FLAG" H 1650 1273 50  0000 C CNN
F 2 "" H 1650 1100 50  0001 C CNN
F 3 "~" H 1650 1100 50  0001 C CNN
	1    1650 1100
	-1   0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5E3AF354
P 1650 1800
F 0 "#FLG02" H 1650 1875 50  0001 C CNN
F 1 "PWR_FLAG" H 1650 1973 50  0000 C CNN
F 2 "" H 1650 1800 50  0001 C CNN
F 3 "~" H 1650 1800 50  0001 C CNN
	1    1650 1800
	1    0    0    1   
$EndComp
Wire Wire Line
	1650 1100 1650 1150
Wire Wire Line
	1650 1150 1425 1150
Connection ~ 1425 1150
Wire Wire Line
	1425 1150 1425 1250
Wire Wire Line
	1650 1800 1650 1750
Wire Wire Line
	1650 1750 1425 1750
Connection ~ 1425 1750
Wire Wire Line
	1425 1750 1425 1800
Text HLabel 1425 1800 3    50   Input ~ 0
Ground
Text HLabel 1425 1100 1    50   Input ~ 0
Supply
$EndSCHEMATC
